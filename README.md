This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Antonios Ntasioudis clothing shop 

### Setup
On root folder:
1. `npm i` (or `yarn`) 
2. `npm test` 
3. `npm start` (or `yarn start`)

### Deployment
You can find the solution deployed here:
` http://tonyshop.s3-website-eu-west-1.amazonaws.com/ `

### Vouchers
List of Vouchers to use

1. 5OFF
2. 10OFF
3. 15OFF

### Rationale - Approach
  The approach of the following solution is to start with extental data depenencies (aka the edge of the app boundary)

  `Product` provider will be responsible for providing the products.
  We will follow similar logic with `Vouchers` but in this case,it  won't return a list of vouchers but will expose a function where you can get a voucher only if it exists.

  Once we complete with the depedencies, we will focus on the main `Shop` which holds all of the business logic and delegates (data and functions) to the child components.
  
 `Basket` and `Product` are stateless components where they destructrure the passed object and present data or call functions as required.

### Code Layout

```bash
-src
  |-initial-react-app
  |-basket/
    |-component
    |-tests
    |-styles
  |-product/
    |-component
    |-tests
  |-providers/
    |-components
    |-tests
    |-data-in-json   
  |-shop/
    |-component (core app)
    |-tests
    |-logic (helper functions)
```


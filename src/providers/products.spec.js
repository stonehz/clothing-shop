import { getProducts } from "./products";

describe("Product Provider", () => {
  it("should return a list of products", () => {
    const products = getProducts();
    expect(products.length).not.toBe(0);
    expect(products.length).toBe(13);
  });
});

import { getVoucher } from "./vouchers";

describe("Voucher provider", () => {
  it("given an invalid voucher, should return an error", () => {
    const validVoucher = getVoucher("blah");
    expect(validVoucher.error).toBe(
      "Your voucher is not valid, please try again."
    );
  });

  it("given a valid voucher, should return the voucher details", () => {
    const validVoucher = getVoucher("5OFF");
    expect(validVoucher.data.code).toBe("5OFF");
    expect(validVoucher.data.valueToDeduct).toBe(5.0);
  });
});

import rawVouchers from "./voucherList.json";

export const getVoucher = code => {
  const matched = rawVouchers.find(voucher => voucher.code === code);
  return {
    data: matched,
    error: matched ? "" : "Your voucher is not valid, please try again."
  };
};

export default getVoucher;

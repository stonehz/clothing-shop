import rawProducts from "./productList.json";

export const getProducts = () => rawProducts;

export default getProducts;

import React from "react";
import "../enzymeHelper";
import { shallow } from "enzyme";
import Basket from "./index";

const initialState = {
  items: [],
  totalPrice: 0,
  removeFromBasket: jest.fn()
};

describe("Basket", () => {
  it("should be empty when it first loads", () => {
    const wrapper = shallow(<Basket {...initialState} />);
    expect(wrapper.find("span.price").text()).toBe("£0.00");
  });
  it("should remove the item when user clicks the associated icon", () => {
    const state = {
      items: [{ name: "foo", id: "item" }],
      totalPrice: 10,
      removeFromBasket: jest.fn()
    };
    const wrapper = shallow(<Basket {...state} />);
    wrapper.find(".item .remove").simulate("click");
    expect(state.removeFromBasket).toHaveBeenCalledWith("item");
  });
});

import React from "react";
import { Collection, CollectionItem, Input } from "react-materialize";
import { showPrice } from "../product";
import "./style.css";
export const Basket = ({
  items,
  totalPrice,
  removeFromBasket,
  validateVoucher,
  error
}) => {
  return (
    <div className="basket">
      <Collection>
        <CollectionItem className="active">Basket</CollectionItem>
        {items.map(item => {
          return (
            <CollectionItem key={item.id} className={item.id}>
              {item.name}
              <span
                className="secondary-content remove"
                onClick={() => removeFromBasket(item.id)}
              >
                <i className="material-icons">remove_circle</i>
              </span>
            </CollectionItem>
          );
        })}

        <CollectionItem>
          Total Price:
          <span className="secondary-content price">
            {showPrice(totalPrice)}
          </span>
        </CollectionItem>
        <CollectionItem className="center-align">
          {error !== "" ? (
            <div className="voucher-msg red lighten-3">{error}</div>
          ) : (
            <div className="voucher-msg">Start typing your voucher</div>
          )}
          <Input
            s={12}
            label="Voucher"
            onChange={e =>
              e.target.value.length > 3 ? validateVoucher(e) : null
            }
          />
        </CollectionItem>
      </Collection>
    </div>
  );
};

export default Basket;

import React from "react";
import { Col, Card, CardTitle, Chip, Button } from "react-materialize";

export const showPrice = price => `£${parseFloat(price).toFixed(2)}`;

export const ProductCard = ({
  id,
  name,
  type,
  price,
  quantity,
  addToBasket
}) => {
  return (
    <Col s={12} m={6} l={3} key={id} className={`product ${id}`}>
      <Card
        className="tiny blue-grey darken-5"
        textClassName="white-text"
        header={
          <CardTitle image="https://placekitten.com/260/160">{name}</CardTitle>
        }
        actions={[
          <Button
            key={id}
            disabled={quantity === 0}
            waves="light"
            onClick={() => addToBasket(id)}
          >
            {quantity === 0 ? "OUT OF STOCK" : "ADD"}
          </Button>
        ]}
      >
        {type.map(cat => {
          return <Chip key={cat}> {cat} </Chip>;
        })}
        <hr />
        {quantity} left @ {showPrice(price)}
      </Card>
    </Col>
  );
};
export default ProductCard;

import React from "react";
import "../enzymeHelper";
import { mount } from "enzyme";
import ProductCard from "./index";

const initialState = {
  id: "foo",
  name: "Product",
  type: ["foo", "bar"],
  price: 20.0,
  quantity: 2,
  addToBasket: jest.fn()
};

describe("Product", () => {
  it("should render a product", () => {
    const wrapper = mount(<ProductCard {...initialState} />);
    expect(wrapper.find(".chip").length).toBe(2);
    expect(wrapper.text()).toMatch(/Product foo  bar 2 left @ £20\.00/);
  });
  it("should add to basket when user clicks the button", () => {
    const wrapper = mount(<ProductCard {...initialState} />);
    wrapper.find("button.btn").simulate("click");
    expect(initialState.addToBasket).toHaveBeenCalledWith("foo");
  });
});

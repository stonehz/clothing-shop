import React, { Component } from "react";
import { Navbar, NavItem, Footer } from "react-materialize";
import Shop from "./shop";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar brand="Tony Clothing" right>
          <NavItem href="/">Home</NavItem>
        </Navbar>
        <Shop />
        <Footer copyrights="&copy; 2018 by Antonios Ntasioudis" />
      </div>
    );
  }
}

export default App;

import React from 'react';
import '../enzymeHelper';
import { shallow, mount } from 'enzyme';

import Shop from './index';
import ProductCard from '../product';
import Basket from '../basket';



describe('Main Shop', () => {
  it('should show all products', () => {
    const wrapper = shallow(<Shop />);
    expect(wrapper.find(ProductCard).length).toBe(13);
  });
  it('should show a basket', () => {
    const wrapper = shallow(<Shop />);
    expect(wrapper.find(Basket).length).toBe(1);
  });
  describe('with Products', () => {
    it('should add the selected product to basket state', () => {
      const wrapper = mount(<Shop />);
      wrapper.find('.prod1 button.btn').simulate('click');
      expect(wrapper.state().basket.items.length).toBe(1);
    });
  });
  describe('from Basket Products', () => {
    it('should show the  correct total', () => {
      const wrapper = mount(<Shop />);
      wrapper.find('.prod1 button.btn').simulate('click');
      expect(wrapper.find('.price').text()).toBe('£99.00');
    });
    it('should remove a product and show the correct total', () => {
      const wrapper = mount(<Shop />);
      wrapper.find('.prod1 button.btn').simulate('click');
      wrapper.find('.basket .prod1 .remove').simulate('click');
      expect(wrapper.find('.price').text()).toBe('£0.00');
    });
    describe('with Voucher', () => {
      it('should show me an error on invalid discount', () => {
        const wrapper = mount(<Shop />);
        wrapper.find('.prod1 button.btn').simulate('click');
        wrapper.find('.input-field input').simulate('change', { target: { value: '10123' } });
        expect(wrapper.find('.voucher-msg').text()).toBe('Code did not match or basket empty');
      });
      it('5OFF should reduce the price by 5', () => {
        const wrapper = mount(<Shop />);
        wrapper.find('.prod1 button.btn').simulate('click')
        wrapper.find('.input-field input').simulate('change', { target: { value: '5OFF' } });
        expect(wrapper.find('.price').text()).toBe('£94.00');
      });
      it('10OFF should reduce the price by 10 if total is over 50', () => {
        const wrapper = mount(<Shop />);
        wrapper.find('.prod1 button.btn').simulate('click')
        wrapper.find('.input-field input').simulate('change', { target: { value: '10OFF' } });
        expect(wrapper.find('.price').text()).toBe('£89.00');

        wrapper.find('.basket .prod1 .remove').simulate('click');
        wrapper.find('.prod2 button.btn').simulate('click');
        wrapper.find('.input-field input').simulate('change', { target: { value: '10OFF' } });
        expect(wrapper.find('.price').text()).toBe('£42.00');
      });
      it('15OFF should reduce the price by 15 if total is over 75 and there is at least one foowear', () => {
        const wrapper = mount(<Shop />);
        wrapper.find('.prod1 button.btn').simulate('click')
        wrapper.find('.input-field input').simulate('change', { target: { value: '15OFF' } });
        expect(wrapper.find('.price').text()).toBe('£84.00');
      });
    });
  });
})
import React, { Component } from 'react';
import { Row, Col } from 'react-materialize';
import ProductCard from '../product';
import Basket from '../basket';
import { getProducts } from '../providers/products';
import { getVoucher } from '../providers/vouchers';
import { calculateTotal, deleteBasketItem, applyVoucher } from './logic';

const initialState = {
  products: [],
  basket: {
    totalPrice: 0.00,
    items: [],
  },
  error: "",
}

class Shop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      products: getProducts(),
    };
    this.addToBasket = this.addToBasket.bind(this);
    this.removeFromBasket = this.removeFromBasket.bind(this);
    this.validateVoucher = this.validateVoucher.bind(this);
  }
  addToBasket(id) {
    const found = this.state.products.find((prod) => prod.id === id);
    this.setState(prevState => {
      return {
        basket: {
          items: [...prevState.basket.items, found],
          totalPrice: calculateTotal([...prevState.basket.items, found]),
        },
      };
    });
  }
  removeFromBasket(id) {
    const found = this.state.products.find((prod) => prod.id === id);
    this.setState(prevState => {
      return {
        basket: {
          items: deleteBasketItem(prevState.basket.items, found),
          totalPrice: calculateTotal(deleteBasketItem(prevState.basket.items, found)),
        },
      };
    });
  }
  validateVoucher(e) {
    const voucher = getVoucher(e.target.value);
    if (voucher.data && this.state.basket.items.length > 0) {
      this.setState(prevState => {
        return {
          basket: {
            ...prevState.basket,
            totalPrice: applyVoucher(voucher, this.state.basket),
          },
          error: ""
        };
      });
    }
    else {
      this.setState(prevState => {
        return {
          error: "Code did not match or basket empty"
        };
      });
    }

  }

  render() {
    const { products, basket, error } = this.state;
    return (
      <div className="content main">
        <Row>
          <Col l={10} s={12}>
            {products.map((product) => { return <ProductCard key={product.id} {...product} addToBasket={this.addToBasket} /> })}
          </Col>
          <Col l={2} s={12}>
            <Basket
              {...basket}
              removeFromBasket={this.removeFromBasket}
              validateVoucher={this.validateVoucher}
              error={error}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Shop;

const reduceSum = (total, { price }) => total + price;

export const calculateTotal = items => {
  return items.reduce(reduceSum, 0);
};

export const deleteBasketItem = (state, toDelete) => {
  let rest;
  [toDelete, ...rest] = state;
  return rest;
};

export const applyVoucher = ({ data }, { items, totalPrice }) => {
  const canBeApplied = data.type.map((subtype) => {
    switch (subtype) {
      case 'fixed':
        return totalPrice > 5
      case 'totalBased':
        return totalPrice > 50
      case 'footwearBased':
        return !!(items.length > 0 && items.find(({ type }) => type.includes('footwear')));
      default:
        return false
    }
  });
  return canBeApplied.includes(false) ? totalPrice : (totalPrice - data.valueToDeduct);
};
